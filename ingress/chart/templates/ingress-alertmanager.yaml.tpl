apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
  name: alertmanager
spec:
  rules:
    - host: alertmanager.{{ .Values.clusterDomain }}
      http:
        paths:
          - backend:
              service:
                name: kube-prometheus-stack-alertmanager
                port:
                  number: 9093
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - alertmanager.{{ .Values.clusterDomain }}
      secretName: "{{ .Values.tlsSecretName }}"
