apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
  name: grafana
spec:
  rules:
    - host: grafana.{{ .Values.clusterDomain }}
      http:
        paths:
          - backend:
              service:
                name: kube-prometheus-stack-grafana
                port:
                  number: 80
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - grafana.{{ .Values.clusterDomain }}
      secretName: "{{ .Values.tlsSecretName }}"
