#!/bin/bash
set -euxo pipefail

timeout 90 bash -c "until kubectl -n monitoring get po -l app.kubernetes.io/name=metrics-server; do sleep 1; done"
kubectl wait pods -n monitoring -l app.kubernetes.io/name=metrics-server --for condition=Ready --timeout=90s
sleep 15
kubectl top nodes
