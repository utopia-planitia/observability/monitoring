#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

PROMTAIL_CONFIG="$(kubectl --namespace monitoring get secret/promtail -o jsonpath='{.data.promtail\.yaml}' | base64 -d)"
LOKI_URL="$(echo "${PROMTAIL_CONFIG:?}" | grep -m 1 'url:' | awk -F'url: ' '{ print $2 }')"

# when you split "http://loki:3100/" at the delimiter "/"
# then the 1st field is "http:", the 2nd is empty and
# the 3rd field is what we want: "loki:3100"
LOKI_ADDRESS="$(echo "${LOKI_URL:?}" | cut -d / -f 3)"

LOKI_HOST="${LOKI_ADDRESS%:*}"
LOKI_PORT="${LOKI_ADDRESS#*:}"

if test "${LOKI_HOST:?}" -eq "${LOKI_PORT:?}"; then
  LOKI_PORT=80
fi

exec kubectl --namespace monitoring exec daemonsets/promtail -- timeout 1 bash -c "cat </dev/null >/dev/tcp/${LOKI_HOST:?}/${LOKI_PORT:?}"
