apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    app: kube-prometheus-stack
    release: "kube-prometheus-stack"
  name: rules
  namespace: monitoring
spec:
  groups:
    - name: ./rules.rules
      rules:
        - alert: NamespaceTerminatingError
          expr: kube_namespace_status_phase{phase="Terminating"}==1
          for: 60m
          labels:
            severity: warning
          annotations:
            message: Namespace {{ `{{` }} $labels.namespace }} Termination problem
        - alert: DockerHubPullLimit
          expr: dockerhub_limit_remaining_requests_total * on(namespace,pod) group_left(node) kube_pod_info < 50
          for: 60m
          labels:
            severity: warning
          annotations:
            message: |
              {{`Node "{{ $labels.node }}" has only "{{ $value }}" image pulls from Docker Hub left.`}}
        - alert: NodeDown
          annotations:
            message: Instance {{ `{{` }} $labels.instance }} of job {{ `{{` }} $labels.job }} has been down for over 1 minute
          expr: up == 0
          for: 5m
          labels:
            severity: critical
        - alert: HighLoad
          expr: node_load15 / 8 > 2
          for: 30m
          labels:
            severity: warning
          annotations:
            message: CPU per Core higher then 2 (instance {{ `{{` }} $labels.instance }})
        - alert: LoadOutsideCPU
          expr: node_load15 / (count without (cpu, mode) (node_cpu_seconds_total{mode!="idle"})) > 0.1
          for: 30m
          labels:
            severity: warning
          annotations:
            message: High Load with low CPU (instance {{ `{{` }} $labels.instance }})
        - alert: OutOfMemory
          expr: (node_memory_MemFree_bytes+node_memory_Cached_bytes+node_memory_Buffers_bytes)/node_memory_MemTotal_bytes*100<10
          for: 30m
          labels:
            severity: warning
          annotations:
            message: Out of memory (instance {{ `{{` }} $labels.instance }})
        - alert: CPUTempWarning
          expr: node_hwmon_temp_celsius > 70
          for: 5m
          labels:
            severity: critical
          annotations:
            message: CPU Core Temp to high
        - alert: OutOfDiskSpace
          expr: node_filesystem_free_bytes{mountpoint ="/"}/node_filesystem_size_bytes{mountpoint ="/"}*100<10
          for: 30m
          labels:
            severity: warning
          annotations:
            message: Out of disk space (instance {{ `{{` }} $labels.instance }})
        - alert: PrometheusTSDBCompactionsFailing
          annotations:
            description: Prometheus {{`{{`}}$labels.namespace{{`}}`}}/{{`{{`}}$labels.pod{{`}}`}} has detected {{`{{`}}$value | humanize{{`}}`}} compaction failures over the last 3h.
            runbook_url: {{ .Values.rules.PrometheusTSDBCompactionsFailing.runbook_url }}
            summary: Prometheus has issues compacting blocks.
          expr: increase(prometheus_tsdb_compactions_failed_total{job="{{ printf "%s-prometheus" .Release.Name }}",namespace="{{ .Release.Namespace }}"}[3h]) > 0
          for: 4h
          labels:
            severity: warning
