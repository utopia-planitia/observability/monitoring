# Monitoring for Kubernetes

This repo deploys Prometheus and Grafana to collect and show metrics.

Loki collects logs and makes them accessible via Grafana.

Alertmanager ensures service levels are met.
