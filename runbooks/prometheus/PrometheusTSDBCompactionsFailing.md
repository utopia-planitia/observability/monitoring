# PrometheusTSDBCompactionsFailing

## Meaning

Prometheus has issues compacting blocks of the time series database.

## Impact

Metrics and alerts may be missing or inaccurate.

## Diagnosis

Check the logs of the pod and the memory and storage used by the pod. This can happen if there is a lot of going on in the cluster and prometheus did not manage to compact data.

## Mitigation

Check the upstream runbook: <https://runbooks.prometheus-operator.dev/runbooks/prometheus/prometheustsdbcompactionsfailing/>

Increase Prometheus pod memory if necessary so that it caches more from disk. Try expanding volumes if they are too small or too slow. Change PVC storageClass to a more performant one.

### WAL truncation errors due to file corruption

#### Diagnosis

The pod logs contain an error message like this:

> WAL truncation in Compact: create checkpoint: read segments: corruption in segment /prometheus/wal/00018151 at 72: unexpected full record

#### Mitigation

1. Exec into the pod (or find the mount path of the PersistentVolumeClaim on the host) and delete the corrupted file (in the example above: `rm /prometheus/wal/00018151`).
2. Delete all the WAL files in `/prometheus/wal` that are older than the file deleted in the previous step (for example `rm /prometheus/wal/00018150`).
3. Create empty files in the place of all the deleted files from the previous steps (for example `touch /prometheus/wal/00018150 /prometheus/wal/00018151`).
4. Make sure the file ownership and permissions are the same as with the other WAL files (eg. `chown 1000:2000 /prometheus/wal/00018150 /prometheus/wal/00018151` and `chmod g+w /prometheus/wal/00018150 /prometheus/wal/00018151`).
5. Restart the pod.
6. Depending on how long ago the last successful compaction was, the next compaction might use a lot of memory and take a while. Look out if the pod gets out-of-memory-killed and (temporarily) increase the memory requests and limits of the prometheus container. Disable the startupProbe and the livenessProbe if the container terminates with exit code zero and you see the message "See you next time!" in the logs and a failed startup probe in the pod events (kubectl describe).
